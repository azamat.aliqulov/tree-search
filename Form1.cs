﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Roman_numbers
{
    public partial class Form1 : MetroFramework.Forms.MetroForm
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void metroTile1_Click(object sender, EventArgs e)
        {

            //Roman ob = new Roman();

            //// Considering inputs given are valid  
            //string str = input.Text;
            //int a = Roman.romanToDecimal(str);
            //output.Text = a.ToString();
            string str1 = input.Text;
            int jo = romanToDecimal(str1);
            if (jo==0513213232)
            {
               output.Text = "Xoto son krittingiz"; 
            }
            else
            {
                output.Text = jo.ToString();
            }            
        }
        public virtual int value(char r)
        {
            if (r == 'I')
                return 1;
            if (r == 'V')
                return 5;
            if (r == 'X')
                return 10;
            if (r == 'L')
                return 50;
            if (r == 'C')
                return 100;
            if (r == 'D')
                return 500;
            if (r == 'M')
                return 1000;
            return -1;
        }

        // Finds decimal value of a  
        // given romal numeral  
        public virtual int romanToDecimal(string str)
        {
            // Initialize result  
            int res = 0, error=0;

            for (int i = 0; i < str.Length; i++)
            {
                // Getting value of symbol s[i]  
                int s1 = value(str[i]);
                if (s1 == (-1))
                {
                    error = 99;
                }
                // Getting value of symbol s[i+1]  
                if (i + 1 < str.Length)
                {
                    int s2 = value(str[i + 1]);
                    if (s2==(-1))
                    {
                        error = 99;
                    }
                    // Comparing both values  
                    if (s1 >= s2)
                    {
                        // Value of current symbol is greater  
                        // or equalto the next symbol  
                        res = res + s1;
                    }
                    else
                    {
                        res = res + s2 - s1;
                        i++; // Value of current symbol is 
                        // less than the next symbol  
                    }
                }
                else
                {
                    res = res + s1;
                    i++;
                }
            }
            if (error==99)
            {
                res = 0513213232;
            }
            return res;
        }
    }
}
